FROM scratch
EXPOSE 8080
ENTRYPOINT ["/new-build"]
COPY ./bin/ /